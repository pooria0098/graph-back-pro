from django.shortcuts import get_list_or_404
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.generics import CreateAPIView
from charts.serializers import ExcelSerializer
from nodes.models import Tree, Node, NodeRef

import numpy as np
import os
import pandas as pd
from ast import literal_eval
from openpyxl import load_workbook


def append_df_to_excel(filename,
                       df,
                       sheet_name='Sheet1',
                       startrow=None,
                       truncate_sheet=False,
                       **to_excel_kwargs):
    """
    Append a DataFrame [df] to existing Excel file [filename]
    into [sheet_name] Sheet.
    If [filename] doesn't exist, then this function will create it.

    @param filename: File path or existing ExcelWriter
                     (Example: '/path/to/file.xlsx')
    @param df: DataFrame to save to workbook
    @param sheet_name: Name of sheet which will contain DataFrame.
                       (default: 'Sheet1')
    @param startrow: upper left cell row to dump data frame.
                     Per default (startrow=None) calculate the last row
                     in the existing DF and write to the next row...
    @param truncate_sheet: truncate (remove and recreate) [sheet_name]
                           before writing DataFrame to Excel file
    @param to_excel_kwargs: arguments which will be passed to `DataFrame.to_excel()`
                            [can be a dictionary]
    @return: None

    Usage examples:

    >>> append_df_to_excel('d:/temp/test.xlsx', df)

    >>> append_df_to_excel('d:/temp/test.xlsx', df, header=None, index=False)

    >>> append_df_to_excel('d:/temp/test.xlsx', df, sheet_name='Sheet2',index=False)

    >>> append_df_to_excel('d:/temp/test.xlsx', df, sheet_name='Sheet2',index=False, startrow=25)

    (c) [MaxU](https://stackoverflow.com/users/5741205/maxu?tab=profile)
    """
    # Excel file doesn't exist - saving and exiting
    if not os.path.isfile(filename):
        df.to_excel(
            filename,
            sheet_name=sheet_name,
            startrow=startrow if startrow is not None else 0,
            **to_excel_kwargs)
        return

    # ignore [engine] parameter if it was passed
    if 'engine' in to_excel_kwargs:
        to_excel_kwargs.pop('engine')

    writer = pd.ExcelWriter(filename, engine='openpyxl', mode='a')

    # try to open an existing workbook
    writer.book = load_workbook(filename)

    # get the last row in the existing Excel sheet
    # if it was not specified explicitly
    if startrow is None and sheet_name in writer.book.sheetnames:
        startrow = writer.book[sheet_name].max_row

    # truncate sheet
    if truncate_sheet and sheet_name in writer.book.sheetnames:
        # index of [sheet_name] sheet
        idx = writer.book.sheetnames.index(sheet_name)
        # remove [sheet_name]
        writer.book.remove(writer.book.worksheets[idx])
        # create an empty sheet [sheet_name] using old index
        writer.book.create_sheet(sheet_name, idx)

    # copy existing sheets
    writer.sheets = {ws.title: ws for ws in writer.book.worksheets}

    if startrow is None:
        startrow = 0

    # write out the new sheet
    df.to_excel(writer, sheet_name, startrow=startrow, **to_excel_kwargs)

    # save the workbook
    writer.save()


def meth(data):
    rows = []
    s_row = []
    f_w = []
    d_w = []
    for i in range(0, len(data)):
        a, b, c = 1, 1, 1
        for j in range(0, len(data[i])):
            d, e, f = data[i][j]
            a, b, c = a * d, b * e, c * f
        rows.append((a, b, c))
    L, M, U = 0, 0, 0
    for i in range(0, len(rows)):
        a, b, c = rows[i]
        a, b, c = a ** (1 / len(data)), b ** (1 / len(data)), c ** (1 / len(data))
        s_row.append((a, b, c))
        L = L + a
        M = M + b
        U = U + c
    for i in range(0, len(s_row)):
        a, b, c = s_row[i]
        a, b, c = a * (U ** -1), b * (M ** -1), c * (L ** -1)
        f_w.append((a, b, c))
        d_w.append((a + b + c) / 3)
    n_w = [item / sum(d_w) for item in d_w]
    return f_w, d_w, n_w


def meth2(data):
    w = 0
    sum_row = []
    sum_row2 = []
    weight_final = []
    f_row = []

    l, m, u = 0, 0, 0
    for i in range(0, len(data)):
        a, b, c = 0, 0, 0
        a2, b2, c2 = 0, 0, 0
        for j in range(0, len(data[i])):
            d, e, f = data[i][j]
            a, b, c = a + d, b + e, c + f
        sum_row.append((a, b, c))
        l = l + a
        m = m + b
        u = u + c
        sum_row2.append((l, m, u))
    # rev23
    ll, mm, uu = 0, 0, 0
    ll = u ** -1
    mm = m ** -1
    uu = l ** -1
    for i in range(0, len(data[i])):
        a, b, c = sum_row[i]
        a2, b2, c2 = a * (ll), b * (mm), c * (uu)
        f_row.append((a2, b2, c2))
    weith_s = []
    for j in range(0, len(data)):
        l_main, m_main, u_main = f_row[j]
        valu = []
        for i in range(0, len(data)):
            s = i - j
            s = abs(s)
            v1 = 0.0
            if s > 0:
                l_comp, m_comp, u_comp = f_row[i]
                if m_main >= m_comp:
                    v = 1
                elif l_comp >= u_main:
                    v = 0
                else:
                    v = (l_comp - u_main) / (m_main - u_main) - (m_comp - l_comp)
                valu.append(v)
        weith = np.amin(valu)
        weith_s.append(weith)

    for j in range(0, len(weith_s)):
        q = weith_s[j]
        w = w + q
        weight_f = q / w
        weight_final.append(weight_f)

    return weight_final


class CreateChartView(APIView):
    # renderer_classes = (XLSXRenderer, JSONRenderer)

    def post(self, request):
        # print('self.request.POST: ', self.request.POST)
        # print('self.request.data: ', self.request.data)
        tree_id = self.request.data['tree_id']
        print('tree_id: ', tree_id)
        tree = Tree.objects.get(id=tree_id)
        print('tree: ', tree)
        all_nodes = tree.nodes.all()
        print('all_nodes: ', all_nodes)

        node_list = []
        for node in all_nodes:
            if node.properties.all():
                node_list.append(node)
        print('node_list: ', node_list)

        # ------ last_level ------
        print('last_level')
        cols = rows = tree.nodes.values_list('node_name', flat=True).filter(
            row_name_number=tree.row_numbers - 1).distinct()
        print('cols: ', cols)
        print('rows: ', rows)
        counter = 1
        for node in node_list:
            node_row_number = node.row_name_number
            if node_row_number != tree.row_numbers - 1:
                # print(f'node:{node.node_name} row_name_number:{node_row_number}')
                df = pd.DataFrame('', columns=cols, index=rows)
                if counter == 1:
                    # print('Write')
                    writer = pd.ExcelWriter('data.xlsx', mode="w", engine='openpyxl')
                else:
                    # print('Append')
                    writer = pd.ExcelWriter('data.xlsx', mode="a", engine='openpyxl')

                df.to_excel(excel_writer=writer,
                            sheet_name=f'Row_{str(tree.row_numbers - 1)}_Node_{node.node_name}')
                writer.save()
                counter += 1

        # ------ others_level ------
        print('others_level')
        # node_numbs_dict = {}
        for level in range(tree.row_numbers - 2, 0, -1):  # 3 2
            print('level', level)
            # ----- Create Excel Tables -----
            try:
                if final_node_nums_dict:
                    for obj in final_node_nums_dict:
                        # print('POORIA: ', final_node_nums_dict)
                        # print('OBJ1: ', obj)
                        # print('OBJ2: ', final_node_nums_dict[obj])
                        print('COLS=ROWS: ', final_node_nums_dict[obj]['nodes'])
                        print('LEVEL: ', final_node_nums_dict[obj]["level"])
                        df = pd.DataFrame('', columns=final_node_nums_dict[obj]['nodes'],
                                          index=final_node_nums_dict[obj]['nodes'])
                        df.to_excel(excel_writer=writer,
                                    sheet_name=f'Row_{str(final_node_nums_dict[obj]["level"])}_Node_{obj}')
                        writer.save()
            except:
                pass
            # ----- Create Excel Tables -----

            if level == 1: break
            final_node_nums_dict = {}

            for levell in range(level, 0, -1):
                list = Node.objects.filter(row_name_number=levell - 1, tree_id=tree_id).order_by('row_name_number')
                print('list', list)

                for node in list:
                    print('node', node)
                    if level - node.row_name_number > 1:
                        up_level_nodes = NodeRef.objects.filter(node_ref=node)
                        a = {}
                        print('up_level_nodes', up_level_nodes)

                        for nodde in up_level_nodes:
                            print('nodde.child.node_name: ', nodde.child.node_name)
                            try:
                                a[nodde.child.node_name] = final_node_nums_dict[nodde.child.node_name]
                            except:
                                pass

                        print('a: ', a)
                        # Try Max
                        # node_with_max_value = max(a, key=lambda x: a[x]['counts'])
                        # if a[node_with_max_value]['counts'] > 1:
                        #     # a[node_with_max_value]['level'] = node.row_name_number + 1
                        #     new_dict = {
                        #         'counts': a[node_with_max_value]['counts'],
                        #         'nodes': a[node_with_max_value]['nodes'],
                        #         'level': level
                        #     }
                        #     final_node_nums_dict[node.node_name] = new_dict
                        # Try All
                        for dic in a:
                            print('dic: ', dic)
                            print('a[dic]: ', a[dic])
                            if a[dic]['counts'] > 1:
                                # a[node_with_max_value]['level'] = node.row_name_number + 1
                                new_dict = {
                                    'counts': a[dic]['counts'],
                                    'nodes': a[dic]['nodes'],
                                    'level': level
                                }
                                if node.node_name in final_node_nums_dict.keys():
                                    final_node_nums_dict[node.node_name + '_new'] = new_dict
                                else:
                                    final_node_nums_dict[node.node_name] = new_dict
                        print('new final_node_nums_dict', final_node_nums_dict)
                    else:
                        if node.node_name not in final_node_nums_dict:
                            if NodeRef.objects.filter(node_ref=node).count() > 1:
                                nodes = get_list_or_404(NodeRef.objects.values_list(
                                    'child__node_name', flat=True), node_ref=node)
                                counts = len(nodes)

                                print('original_nodes: ', nodes)
                                nodes.sort(key=int)
                                print('sorted_nodes: ', nodes)

                                final_node_nums_dict[node.node_name] = {
                                    'counts': counts,
                                    'nodes': nodes,
                                    'level': level
                                }
                                print('final_node_nums_dict: ', final_node_nums_dict)

        # ------ first_level ------
        print('first_level')
        cols = rows = tree.nodes.values_list('node_name', flat=True).filter(
            row_name_number=1).distinct()
        print('cols: ', cols)
        print('rows: ', rows)
        df = pd.DataFrame('', columns=cols, index=rows)
        writer = pd.ExcelWriter('data.xlsx', mode="a", engine='openpyxl')
        df.to_excel(excel_writer=writer,
                    sheet_name=f'Row_1_Node_{node_list[0].node_name}')
        writer.save()

        return Response({
            'excel_path': 'http://127.0.0.1:8000/media/data.xlsx'
        })


class CreateChartView_2(APIView):

    def post(self, request):
        # print('self.request.data: ', self.request.data)
        # print('self.request.POST: ', self.request.POST)
        charts = self.request.data
        print('charts: ', charts['chart'])
        charts = charts['chart']
        # charts = FAKE_DATA
        counter = 1

        for chart in charts:
            print('chart: ', chart)

            cells = chart['cells']
            print('cells: ', cells)

            two_d_array = []
            for cell in cells:
                inner_list = []
                for data in cell:
                    fetched_data = data['data'].strip().split(' ')
                    print('fetched_data: ', fetched_data)
                    # joined_data = ','.join(fetched_data)
                    # print('joined_data: ', joined_data)
                    # target_data = '(' + joined_data + ')'
                    # print('target_data: ', target_data)

                    inner_list.append(fetched_data)

                two_d_array.append(inner_list)

            print('two_d_array: ', two_d_array)
            for i in range(len(two_d_array)):
                for j in range(len(two_d_array[i])):
                    if i == j:
                        two_d_array[i][j] = '(' + ','.join(two_d_array[i][j]) + ')'
                    elif i > j:
                        # print('two_d_array[i][j]: ', two_d_array[j][i])
                        reversed_list = []
                        for num in two_d_array[j][i][::-1]:
                            if num == 1:
                                reversed_list.append(num)
                            else:
                                reversed_list.append('%.2f' % (1 / float(num)))
                            # if int(float(num)):
                            #     if num == 1:
                            #         reversed_list.append(num)
                            #     else:
                            #         reversed_list.append('%.2f' % (1 / float(num)))
                            # else:
                            #     print('num else:', num)
                            #     if int(1 / float(num)):
                            #         print('num else if:', num)
                            #         reversed_list.append('%.2f' % (1 / float(num)))
                            #     else:
                            #         print('num else else:', num)
                            #         reversed_list.append('%.2f' % (1 / float(num)))

                        two_d_array[i][j] = reversed_list
                        two_d_array[j][i] = '(' + ','.join(two_d_array[j][i]) + ')'
                        two_d_array[i][j] = '(' + ','.join(two_d_array[i][j]) + ')'

            rows = cols = chart['nodes']
            # print('col=row: ', cols)
            df = pd.DataFrame(two_d_array, columns=cols, index=rows)
            if counter == 1:
                writer = pd.ExcelWriter('data.xlsx', mode="w", engine='openpyxl')
            else:
                writer = pd.ExcelWriter('data.xlsx', mode="a", engine='openpyxl')

            df.to_excel(excel_writer=writer,
                        sheet_name=f'Row_{chart["level"]}_Node_{chart["key"]}')
            writer.save()
            counter += 1

        return Response('200')


class FillChartView(APIView):
    def post(self, request):
        returnedData = []
        tree_id = self.request.data['tree_id']
        # print('tree_id: ', tree_id)
        tree = Tree.objects.get(id=tree_id)
        # print('tree: ', tree)
        all_nodes = tree.nodes.all()
        # print('all_nodes: ', all_nodes)

        node_list = []
        for node in all_nodes:
            if node.properties.all():
                node_list.append(node)
        # print('node_list: ', node_list)

        # ------ last_level ------
        last_level_dict = {}
        last_level_num = tree.row_numbers - 1
        # print('last_level_num: ', last_level_num)
        # last_node_names = tree.nodes.values_list('node_name', flat=True).filter(
        #     row_name_number=tree.row_numbers - 1).distinct()
        last_node_names = get_list_or_404(tree.nodes.values_list('node_name', flat=True).distinct(),
                                          row_name_number=tree.row_numbers - 1)
        # print('last_node_names: ', list(last_node_names))
        last_node_nums = len(last_node_names)
        # print('last_node_nums: ', last_node_nums)

        for node in node_list:
            node_row_number = node.row_name_number
            if node_row_number != tree.row_numbers - 1:
                last_level_dict[node.node_name] = {
                    'counts': last_node_nums,
                    'nodes': last_node_names,
                    'level': last_level_num
                }

        print('last_level_dict: ', last_level_dict)
        returnedData.append(last_level_dict)

        # ------ others_level ------
        for level in range(tree.row_numbers - 2, 0, -1):  # 3 2
            print('level', level)
            if level == 1: break
            final_node_nums_dict = {}
            for levell in range(level, 0, -1):
                list = Node.objects.filter(row_name_number=levell - 1).order_by('row_name_number')
                # print('list', list)

                for node in list:
                    # print('node', node)
                    if level - node.row_name_number > 1:
                        up_level_nodes = NodeRef.objects.filter(node_ref=node)
                        a = {}
                        # print('up_level_nodes', up_level_nodes)

                        for nodde in up_level_nodes:
                            # print('nodde.child.node_name: ', nodde.child.node_name)
                            try:
                                a[nodde.child.node_name] = final_node_nums_dict[nodde.child.node_name]
                            except:
                                pass

                        # print('a: ', a)
                        # Try All
                        for dic in a:
                            # print('dic: ', dic)
                            # print('a[dic]: ', a[dic])
                            if a[dic]['counts'] > 1:
                                # a[node_with_max_value]['level'] = node.row_name_number + 1
                                new_dict = {
                                    'counts': a[dic]['counts'],
                                    'nodes': a[dic]['nodes'],
                                    'level': level
                                }
                                if node.node_name in final_node_nums_dict.keys():
                                    final_node_nums_dict[node.node_name + '_new'] = new_dict
                                else:
                                    final_node_nums_dict[node.node_name] = new_dict
                        print('others_level_dict: ', final_node_nums_dict)
                    else:
                        if node.node_name not in final_node_nums_dict:
                            if NodeRef.objects.filter(node_ref=node).count() > 1:
                                nodes = get_list_or_404(NodeRef.objects.values_list(
                                    'child__node_name', flat=True), node_ref=node)
                                counts = len(nodes)
                                # print('original_nodes: ', nodes)
                                nodes.sort(key=int)
                                # print('sorted_nodes: ', nodes)
                                final_node_nums_dict[node.node_name] = {
                                    'counts': counts,
                                    'nodes': nodes,
                                    'level': level
                                }
                                # print('final_node_nums_dict: ', final_node_nums_dict)
            returnedData.append(final_node_nums_dict)

        # ------ first_level ------
        first_level_dict = {}
        node_names = get_list_or_404(tree.nodes.values_list('node_name', flat=True).distinct(), row_name_number=1)
        # tree.nodes.values_list('node_name', flat=True).filter(
        #     row_name_number=1).distinct()
        node_nums = len(node_names)
        # print('node_list[0]: ', node_list[0].node_name)
        first_level_dict[node_list[0].node_name] = {
            'counts': node_nums,
            'nodes': node_names,
            'level': 1
        }
        print('first_level_dict: ', first_level_dict)
        returnedData.append(first_level_dict)

        print('returnedData: ', returnedData)
        return Response(returnedData)


class SendExcelView(CreateAPIView):
    serializer_class = ExcelSerializer


class DoMeshView(APIView):

    def post(self, request):
        xls = pd.ExcelFile('data.xlsx')
        sheets = xls.sheet_names
        print('sheets: ', sheets)

        for sheet in sheets:
            print('sheetName: ', sheet)
            df = pd.read_excel('data.xlsx', sheet, index_col=0)
            # print('df: ', df)
            # print('df.length', len(df))
            position = len(df)
            print('position: ', position)
            if position == 8:
                print('3x3')
                df = pd.read_excel('data.xlsx', sheet, index_col=0, skipfooter=5)
            if position == 5:
                print('2x2')
                df = pd.read_excel('data.xlsx', sheet, index_col=0, skipfooter=3).fillna('')
                df.replace('nan', '')
                df.replace('NaN', '')
                print('df: ', df)

            excel_array = df.values
            print('excel_array: ', excel_array)
            target_list = []
            for i in range(len(excel_array)):
                new_list = []
                for j in range(len(excel_array[i])):
                    if excel_array[i][j]:
                        new_list.append(literal_eval(excel_array[i][j]))
                target_list.append(new_list)
            print('target_list: ', target_list)

            # Do Meth
            weight, de_weight, no_weight = meth(target_list)
            weight_list = []
            de_weight_list = []
            no_weight_list = []
            for i in range(0, len(weight)):
                weight_list.append(np.around(weight[i], 3))
            for i in range(0, len(de_weight)):
                de_weight_list.append(round(de_weight[i], 3))
            for i in range(0, len(no_weight)):
                no_weight_list.append(round(no_weight[i], 3))
            print('weight_list: ', weight_list)
            print('de_weight_list: ', de_weight_list)
            print('no_weight_list: ', no_weight_list)

            # create row name
            weight_row_name_list = []
            for i in range(len(weight_list)):
                weight_row_name_list.append(f'a{i}')
            de_weight_row_name_list = []
            for i in range(len(de_weight_list)):
                de_weight_row_name_list.append(f'g{i}')
            no_weight_row_name_list = []
            for i in range(len(no_weight_list)):
                no_weight_row_name_list.append(f'g{i}')
            # create row name

            weight_list_df = pd.DataFrame(weight_list,
                                          index=weight_row_name_list,
                                          columns=['weight', 'weight', 'weight'])
            de_weight_list_df = pd.DataFrame(de_weight_list,
                                             index=de_weight_row_name_list,
                                             columns=['de_weight'])
            no_weight_list_df = pd.DataFrame(no_weight_list,
                                             index=no_weight_row_name_list,
                                             columns=['no_weight'])

            print('weight_list_df: ', weight_list_df)
            print('de_weight_list_df: ', de_weight_list_df)
            print('no_weight_list_df: ', no_weight_list_df)

            start_row_weight = position + 2
            start_row_de_weight = start_row_weight + len(weight_list) + 2
            start_row_no_weight = start_row_de_weight + len(de_weight_list) + 2
            append_df_to_excel('data.xlsx', weight_list_df,
                               sheet_name=sheet,
                               startrow=start_row_weight)
            append_df_to_excel('data.xlsx', de_weight_list_df,
                               sheet_name=sheet,
                               startrow=start_row_de_weight)
            append_df_to_excel('data.xlsx', no_weight_list_df,
                               sheet_name=sheet,
                               startrow=start_row_no_weight)

        return Response('Created')
        # """
        # data=['weight','a','b','c']
        # df = pd.DataFrame(data)
        # append_df_to_excel('data.xlsx',df, sheet_name='Row_4_Node_root', startrow=10)
        # """


class DoMeshView_2(APIView):

    def post(self, request):
        xls = pd.ExcelFile('data.xlsx')
        sheets = xls.sheet_names
        print('sheets: ', sheets)

        for sheet in sheets:
            print('sheetName: ', sheet)
            df = pd.read_excel('data.xlsx', sheet, index_col=0)
            # print('df: ', df)
            position = len(df)
            print('position: ', position)
            if position == 18:
                print('3x3')
                df = pd.read_excel('data.xlsx', sheet, index_col=0, skipfooter=15)
            if position == 14:
                print('2x2')
                df = pd.read_excel('data.xlsx', sheet, index_col=0, skipfooter=12).fillna('')
                df.replace('nan', '')
                df.replace('NaN', '')
                print('df: ', df)

            excel_array = df.values
            print('excel_array: ', excel_array)
            target_list = []
            for i in range(len(excel_array)):
                new_list = []
                for j in range(len(excel_array[i])):
                    if excel_array[i][j]:
                        new_list.append(literal_eval(excel_array[i][j]))
                target_list.append(new_list)
            print('target_list: ', target_list)

            # Do Meth_2
            weight = meth2(target_list)  # [1.0, 0.06502757210133474, 0.0]
            print('weight: ', weight)
            # print('len: ', len(weight[0]))

            if position == 14 or position == 2:
                print('inside if')
                # new_weight = []
                header_list = ['weight'] * len(weight)
                # for wgt in weight:
                #     new_weight.append([wgt])
                # print('new_weight: ', new_weight)
                weight_df = pd.DataFrame(
                    [weight],
                    index=['t0'],
                    columns=header_list
                )
                # weight_df = pd.DataFrame(weight,
                #                          index=['t0'],
                #                          columns=header_list)
            else:
                print('inside else')
                header_list = []
                for i in range(len(weight)):
                    header_list.append(f't{i}')
                weight_df = pd.DataFrame(weight,
                                         index=header_list,
                                         columns=['weight'])

            print('weight_df: ', weight_df)

            start_row_weight = position + 2

            append_df_to_excel('data.xlsx', weight_df,
                               sheet_name=sheet,
                               startrow=start_row_weight)

        return Response('Created')


# https://github.com/Apkawa/xlsx2html
# from xlsx2html import xlsx2html
# xlsx2html('/home/pt-ubuntu-os/Desktop/graph-back-pro-master/data.xlsx',
#           '/home/pt-ubuntu-os/Desktop/graph-back-pro-master/data.html')

# import pandas as pd
# wb = pd.read_excel('/home/pt-ubuntu-os/Desktop/graph-back-pro-master/data.xlsx')
# wb.to_html('/home/pt-ubuntu-os/Desktop/graph-back-pro-master/data.html')

class ShowTablesForAlgOne(APIView):
    def get(self, request):
        results = []
        xls = pd.ExcelFile('data.xlsx')
        print('xls: ', xls)
        sheets = xls.sheet_names
        print('sheets: ', sheets)
        for sheet in sheets:
            print('sheet: ', sheet)
            df = pd.read_excel('data.xlsx', sheet)

            if len(df) == 18:
                data = xls.parse(sheet, skiprows=0, skipfooter=15).dropna(axis=1, how='all')
                alg_one = xls.parse(sheet, skiprows=5, skipfooter=10).dropna(axis=1, how='all')
                alg_two = xls.parse(sheet, skiprows=10, skipfooter=5).dropna(axis=1, how='all')
                alg_three = xls.parse(sheet, skiprows=15, skipfooter=0).dropna(axis=1, how='all')
                res = {
                    'sheet_name': sheet,
                    'data': {
                        'headers': data.columns.to_list(),
                        'values': data.values.tolist(),
                    },
                    'alg_one': {
                        'headers': alg_one.columns.to_list(),
                        'values': alg_one.values.tolist(),
                    },
                    'alg_two': {
                        'headers': alg_two.columns.to_list(),
                        'values': alg_two.values.tolist(),
                    },
                    'alg_three': {
                        'headers': alg_three.columns.to_list(),
                        'values': alg_three.values.tolist(),
                    }
                }
            else:
                data = xls.parse(sheet, skiprows=0, skipfooter=12).dropna(axis=1, how='all')
                alg_one = xls.parse(sheet, skiprows=4, skipfooter=8).dropna(axis=1, how='all')
                alg_two = xls.parse(sheet, skiprows=8, skipfooter=4).dropna(axis=1, how='all')
                alg_three = xls.parse(sheet, skiprows=12, skipfooter=0).dropna(axis=1, how='all')
                res = {
                    'sheet_name': sheet,
                    'data': {
                        'headers': data.columns.to_list(),
                        'values': data.values.tolist(),
                    },
                    'alg_one': {
                        'headers': alg_one.columns.to_list(),
                        'values': alg_one.values.tolist(),
                    },
                    'alg_two': {
                        'headers': alg_two.columns.to_list(),
                        'values': alg_two.values.tolist(),
                    },
                    'alg_three': {
                        'headers': alg_three.columns.to_list(),
                        'values': alg_three.values.tolist(),
                    }
                }

            results.append(res)

        return Response(results)


class ShowTablesForAlgTwo(APIView):
    def get(self, request):
        results = []
        xls = pd.ExcelFile('data.xlsx')
        print('xls: ', xls)
        sheets = xls.sheet_names
        print('sheets: ', sheets)

        for sheet in sheets:
            print('sheet: ', sheet)
            df = pd.read_excel('data.xlsx', sheet)

            if len(df) == 8:
                data = xls.parse(sheet, skiprows=0, skipfooter=5).dropna(axis=1, how='all')
                print('data8: ', data)
                alg_one = xls.parse(sheet, skiprows=5, skipfooter=0).dropna(axis=1, how='all')
                print('alg8', alg_one)
                res = {
                    'sheet_name': sheet,
                    'data': {
                        'headers': data.columns.to_list(),
                        'values': data.values.tolist(),
                    },
                    'alg_one': {
                        'headers': alg_one.columns.to_list(),
                        'values': alg_one.values.tolist(),
                    },
                }
            else:
                data = xls.parse(sheet, skiprows=0, skipfooter=5).dropna(axis=1, how='all')
                print('data7: ', data)
                alg_one = xls.parse(sheet, skiprows=5, skipfooter=0).dropna(axis=1, how='all')
                print('alg7', alg_one)
                res = {
                    'sheet_name': sheet,
                    'data': {
                        'headers': data.columns.to_list(),
                        'values': data.values.tolist(),
                    },
                    'alg_one': {
                        'headers': alg_one.columns.to_list(),
                        'values': alg_one.values.tolist(),
                    },
                }

            results.append(res)

        return Response(results)


class ShowTablesForBothAlgorithm(APIView):
    def get(self, request):
        results = []
        xls = pd.ExcelFile('data.xlsx')
        print('xls: ', xls)
        sheets = xls.sheet_names
        print('sheets: ', sheets)

        for sheet in sheets:
            print('sheet: ', sheet)
            df = pd.read_excel('data.xlsx', sheet)
            print('len(df): ', len(df))
            if len(df) == 23:
                data = xls.parse(sheet, skiprows=0, skipfooter=20).dropna(axis=1, how='all')
                print('data23: ', data)
                alg_one = xls.parse(sheet, skiprows=5, skipfooter=15).dropna(axis=1, how='all')
                print('alg_one23: ', alg_one)
                alg_two = xls.parse(sheet, skiprows=10, skipfooter=10).dropna(axis=1, how='all')
                print('alg_two23: ', alg_two)
                alg_three = xls.parse(sheet, skiprows=15, skipfooter=5).dropna(axis=1, how='all')
                print('alg_three23: ', alg_three)
                alg_four = xls.parse(sheet, skiprows=20, skipfooter=0).dropna(axis=1, how='all')
                print('alg_four23: ', alg_four)

                res = {
                    'sheet_name': sheet,
                    'data': {
                        'headers': data.columns.to_list(),
                        'values': data.values.tolist(),
                    },
                    'alg_one': {
                        'headers': alg_one.columns.to_list(),
                        'values': alg_one.values.tolist(),
                    },
                    'alg_two': {
                        'headers': alg_two.columns.to_list(),
                        'values': alg_two.values.tolist(),
                    },
                    'alg_three': {
                        'headers': alg_three.columns.to_list(),
                        'values': alg_three.values.tolist(),
                    },
                    'alg_four': {
                        'headers': alg_four.columns.to_list(),
                        'values': alg_four.values.tolist(),
                    }
                }
            else:
                cnd = xls.parse(sheet, skiprows=4, skipfooter=12).dropna(axis=1, how='all')
                if len(cnd.columns) == 4:
                    data = xls.parse(sheet, skiprows=0, skipfooter=15).dropna(axis=1, how='all')
                    print('data17: ', data)
                    alg_one = xls.parse(sheet, skiprows=4, skipfooter=11).dropna(axis=1, how='all')
                    print('alg_one17: ', alg_one)
                    alg_two = xls.parse(sheet, skiprows=8, skipfooter=7).dropna(axis=1, how='all')
                    print('alg_two17: ', alg_two)
                    alg_three = xls.parse(sheet, skiprows=12, skipfooter=3).dropna(axis=1, how='all')
                    print('alg_three17: ', alg_three)
                    alg_four = xls.parse(sheet, skiprows=16, skipfooter=0).dropna(axis=1, how='all')
                    print('alg_four17: ', alg_four)
                else:
                    data = xls.parse(sheet, skiprows=0, skipfooter=15).dropna(axis=1, how='all')
                    print('data17: ', data)
                    alg_one = xls.parse(sheet, skiprows=4, skipfooter=12).dropna(axis=1, how='all')
                    print('alg_one17: ', alg_one)
                    alg_two = xls.parse(sheet, skiprows=7, skipfooter=8).dropna(axis=1, how='all')
                    print('alg_two17: ', alg_two)
                    alg_three = xls.parse(sheet, skiprows=11, skipfooter=4).dropna(axis=1, how='all')
                    print('alg_three17: ', alg_three)
                    alg_four = xls.parse(sheet, skiprows=15, skipfooter=0).dropna(axis=1, how='all')
                    print('alg_four17: ', alg_four)

                res = {
                    'sheet_name': sheet,
                    'data': {
                        'headers': data.columns.to_list(),
                        'values': data.values.tolist(),
                    },
                    'alg_one': {
                        'headers': alg_one.columns.to_list(),
                        'values': alg_one.values.tolist(),
                    },
                    'alg_two': {
                        'headers': alg_two.columns.to_list(),
                        'values': alg_two.values.tolist(),
                    },
                    'alg_three': {
                        'headers': alg_three.columns.to_list(),
                        'values': alg_three.values.tolist(),
                    },
                    'alg_four': {
                        'headers': alg_four.columns.to_list(),
                        'values': alg_four.values.tolist(),
                    }
                }

            results.append(res)

        return Response(results)
