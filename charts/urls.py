from django.urls import path

from .views import (
    CreateChartView,
    FillChartView,
    DoMeshView,
    SendExcelView,
    CreateChartView_2,
    ShowTablesForAlgOne,
    ShowTablesForAlgTwo,
    DoMeshView_2,
    ShowTablesForBothAlgorithm
)

urlpatterns = [
    path('chart/create/', CreateChartView.as_view()),
    path('chart/create/web/', CreateChartView_2.as_view()),
    path('chart/fill/', FillChartView.as_view()),
    path('chart/mesh/', DoMeshView.as_view()),
    path('chart/mesh_2/', DoMeshView_2.as_view()),
    path('chart/send_excel/', SendExcelView.as_view()),
    path('chart/alg_one/', ShowTablesForAlgOne.as_view()),
    path('chart/alg_two/', ShowTablesForAlgTwo.as_view()),
    path('chart/alg_both/', ShowTablesForBothAlgorithm.as_view()),
]
