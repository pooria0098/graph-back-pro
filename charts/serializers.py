from rest_framework import serializers

from charts.models import Chart


class ExcelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Chart
        fields = '__all__'
