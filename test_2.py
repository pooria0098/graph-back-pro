import pandas as pd

results = []
xls = pd.ExcelFile('data.xlsx')
sheets = xls.sheet_names
for sheet in sheets:
    df = pd.read_excel('data.xlsx', sheet)

    if len(df) == 8:
        data = xls.parse(sheet, skiprows=0, skipfooter=5).dropna(axis=1, how='all')
        print('data8: ', data)
        alg_one = xls.parse(sheet, skiprows=5, skipfooter=0).dropna(axis=1, how='all')
        print('alg8', alg_one)
        res = {
            'sheet_name': sheet,
            'data': {
                'headers': data.columns.to_list(),
                'values': data.values.tolist(),
            },
            'alg_one': {
                'headers': alg_one.columns.to_list(),
                'values': alg_one.values.tolist(),
            },
        }
    else:
        data = xls.parse(sheet, skiprows=0, skipfooter=5).dropna(axis=1, how='all')
        print('data7: ', data)
        alg_one = xls.parse(sheet, skiprows=5, skipfooter=0).dropna(axis=1, how='all')
        print('alg7', alg_one)
        res = {
            'sheet_name': sheet,
            'data': {
                'headers': data.columns.to_list(),
                'values': data.values.tolist(),
            },
            'alg_one': {
                'headers': alg_one.columns.to_list(),
                'values': alg_one.values.tolist(),
            },
        }

    results.append(res)

print('results: ', results)
