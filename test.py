from ast import literal_eval
import math

import numpy as np
import pandas as pd

# xls = pd.ExcelFile('data.xlsx')
# sheets = xls.sheet_names
# df = pd.read_excel('data.xlsx', 'Row_4_Node_11')
# print('df', df)
# excel_array = df.values
# print('excel_array: ', excel_array)
# target_lists = []
# for i in range(len(excel_array)):
#     new_list = []
#     for j in range(len(excel_array[i])):
#         new_list.append(excel_array[i][j])
#     target_lists.append(new_list)
# print('target_lists: ', target_lists)
#
#
# def check_if_all_nan(item_list):
#     for item in item_list:
#         if item == item:
#             return False
#     return True
#
#
# one_d_list = []
# two_d_list = []
# nan_number = 0
# for item_list in target_lists:
#     # print('item_list: ', item_list)
#     one_d_list.append(item_list)
#     for item in item_list:
#         if item != item:
#             nan_number += 1
#
#         if nan_number == len(item_list):
#             print('nan_number: ', nan_number)
#             print('item_list: ', item_list)
#             print('one_d_list: ', one_d_list)
#             two_d_list.append(one_d_list[:len(one_d_list) - 1])
#             one_d_list = []
#             nan_number = 0
#
# print('two_d_list: ', two_d_list)

results = []
xls = pd.ExcelFile('data.xlsx')
sheets = xls.sheet_names
for sheet in sheets:
    df = pd.read_excel('data.xlsx', sheet)

    if len(df) == 18:
        data = xls.parse(sheet, skiprows=0, skipfooter=15).dropna(axis=1, how='all')
        alg_one = xls.parse(sheet, skiprows=5, skipfooter=10).dropna(axis=1, how='all')
        alg_two = xls.parse(sheet, skiprows=10, skipfooter=5).dropna(axis=1, how='all')
        alg_three = xls.parse(sheet, skiprows=15, skipfooter=0).dropna(axis=1, how='all')
        res = {
            'sheet_name': sheet,
            'data': {
                'headers': data.columns.to_list(),
                'values': data.values.tolist(),
            },
            'alg_one': {
                'headers': alg_one.columns.to_list(),
                'values': alg_one.values.tolist(),
            },
            'alg_two': {
                'headers': alg_two.columns.to_list(),
                'values': alg_two.values.tolist(),
            },
            'alg_three': {
                'headers': alg_three.columns.to_list(),
                'values': alg_three.values.tolist(),
            }
        }
    else:
        data = xls.parse(sheet, skiprows=0, skipfooter=12).dropna(axis=1, how='all')
        alg_one = xls.parse(sheet, skiprows=4, skipfooter=8).dropna(axis=1, how='all')
        alg_two = xls.parse(sheet, skiprows=8, skipfooter=4).dropna(axis=1, how='all')
        alg_three = xls.parse(sheet, skiprows=12, skipfooter=0).dropna(axis=1, how='all')
        res = {
            'sheet_name': sheet,
            'data': {
                'headers': data.columns.to_list(),
                'values': data.values.tolist(),
            },
            'alg_one': {
                'headers': alg_one.columns.to_list(),
                'values': alg_one.values.tolist(),
            },
            'alg_two': {
                'headers': alg_two.columns.to_list(),
                'values': alg_two.values.tolist(),
            },
            'alg_three': {
                'headers': alg_three.columns.to_list(),
                'values': alg_three.values.tolist(),
            }
        }

    results.append(res)


print('results: ',results)