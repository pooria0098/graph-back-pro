# Generated by Django 3.2.7 on 2021-10-02 11:58

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Node',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('node_name', models.CharField(max_length=256)),
                ('ref_node', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='nodes.node')),
            ],
        ),
        migrations.CreateModel(
            name='Tree',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tree_name', models.CharField(max_length=256)),
            ],
        ),
        migrations.CreateModel(
            name='Row',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('row_name_number', models.IntegerField(choices=[(0, 'Row 0'), (1, 'Row 1'), (2, 'Row 2'), (3, 'Row 3'), (4, 'Row 4'), (5, 'Row 5'), (6, 'Row 6'), (7, 'Row 7'), (8, 'Row 8'), (9, 'Row 9'), (10, 'Row 10')], default=0)),
                ('tree', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='rows', to='nodes.tree')),
            ],
        ),
        migrations.CreateModel(
            name='NodeProperty',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('property_name', models.CharField(max_length=256)),
                ('node', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='properties', to='nodes.node')),
            ],
        ),
        migrations.AddField(
            model_name='node',
            name='row',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='nodes', to='nodes.row'),
        ),
    ]
