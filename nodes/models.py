from django.db import models

ROWS = (
    (0, 'Row 0'),
    (1, 'Row 1'),
    (2, 'Row 2'),
    (3, 'Row 3'),
    (4, 'Row 4'),
    (5, 'Row 5'),
    (6, 'Row 6'),
    (7, 'Row 7'),
    (8, 'Row 8'),
    (9, 'Row 9'),
    (10, 'Row 10')
)


# DIRECTIONS = (
#     ('left', 'LEFT'),
#     ('right', 'RIGHT'),
#     ('both', 'BOTH'),
# )


class Tree(models.Model):
    tree_name = models.CharField(max_length=256)
    row_numbers = models.IntegerField(default=4)

    # def calc(self):
    #     row_numbers = Tree.objects.get().row_numbers
    #     for i in range(row_numbers):
    #         if i == 0:
    #             left = Node.objects.filter(ref_node_id=187, node_direction='left', row_name_number=1)
    #             right = Node.objects.filter(ref_node_id=187, node_direction='right', row_name_number=1)
    #             both = Node.objects.filter(ref_node_id=187, node_direction='both', row_name_number=1)
    #             print('left 0: ', left)
    #             print('right 0: ', right)
    #             print('both 0: ', both)
    #         if i == 1:
    #             left = Node.objects.filter(ref_node__ref_node_id=187, node_direction='left',
    #                                        row_name_number=2)
    #             right = Node.objects.filter(ref_node__ref_node_id=187, node_direction='right',
    #                                         row_name_number=2)
    #             both = Node.objects.filter(ref_node__ref_node_id=187, node_direction='both',
    #                                        row_name_number=2)
    #             print('left 1: ', left)
    #             print('right 1: ', right)
    #             print('both 1: ', both)
    #         if i == 2:
    #             left = Node.objects.filter(ref_node__ref_node__ref_node_id=187, node_direction='left',
    #                                        row_name_number=3)
    #             right = Node.objects.filter(ref_node__ref_node__ref_node_id=187, node_direction='right',
    #                                         row_name_number=3)
    #             both = Node.objects.filter(ref_node__ref_node__ref_node_id=187, node_direction='both',
    #                                        row_name_number=3)
    #             print('left 2: ', left)
    #             print('right 2: ', right)
    #             print('both 2: ', both)
    #         if i == 3:
    #             left = Node.objects.filter(ref_node__ref_node__ref_node__ref_node_id=187, node_direction='left',
    #                                        row_name_number=4)
    #             right = Node.objects.filter(ref_node__ref_node__ref_node__ref_node_id=187, node_direction='right',
    #                                         row_name_number=4)
    #             both = Node.objects.filter(ref_node__ref_node__ref_node__ref_node_id=187, node_direction='both',
    #                                        row_name_number=4)
    #             print('left 3: ', left)
    #             print('right 3: ', right)
    #             print('both 3: ', both)
    #         if i == 4:
    #             pass
    #         if i == 5:
    #             pass
    #         if i == 6:
    #             pass
    #         if i == 7:
    #             pass
    #         if i == 8:
    #             pass
    #         if i == 9:
    #             pass
    #         if i == 10:
    #             pass

    # def calc2(self):
    #     all_nodes = self.nodes.all()
    #     print('all_nodes: ', all_nodes)
    #
    #     node_list = []
    #     for node in all_nodes:
    #         if node.properties.all():
    #             node_list.append(node)
    #
    #     print('last_level')
    #     for node in node_list:
    #         if node.row_name_number != self.row_numbers - 1:
    #             print(node)

    def __str__(self):
        return self.tree_name


class NodeRef(models.Model):
    node_ref = models.ForeignKey('Node', related_name='refs', null=True, blank=True, on_delete=models.CASCADE)
    child = models.ForeignKey('Node', null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        # return f'Node: {self.node_ref.node_name if self.node_ref else None} --> Child: {self.child.node_name if self.child else None}'
        return self.child.node_name if self.child else None


class Node(models.Model):
    tree = models.ForeignKey(Tree, related_name='nodes', on_delete=models.CASCADE)
    row_name_number = models.IntegerField(choices=ROWS, default=0)
    # ref_node = models.ForeignKey(NodeRef, null=True, blank=True, on_delete=models.SET_NULL)
    # row = models.ForeignKey(Row, related_name='nodes', on_delete=models.CASCADE)
    node_name = models.CharField(max_length=256)
    node_horizontal_position = models.IntegerField(default=150)

    # node_direction = models.CharField(choices=DIRECTIONS, max_length=5, null=True, blank=True)

    # node_horizontal_position = models.DecimalField(default=150, decimal_places=2, max_digits=8)

    def __str__(self):
        return f'Node: {self.node_name} --> Row: {self.row_name_number} --> Tree: {self.tree.tree_name}'
        # return f'Node: {self.node_name} --> Ref: {self.ref_node.node_name if self.ref_node else None} --> Row: {
        # self.row_name_number} --> Tree: {self.tree.tree_name} '


class NodeProperty(models.Model):
    node = models.ForeignKey(Node, related_name='properties', on_delete=models.CASCADE)
    property_name = models.CharField(max_length=256)

    def __str__(self):
        return f'{self.property_name} --> {self.node}'

