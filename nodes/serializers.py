from rest_framework import serializers
from rest_framework_bulk import BulkSerializerMixin, BulkListSerializer

from nodes.models import Tree, Node, NodeProperty, NodeRef


# NODE PROPERTY
class ListNodePropertySerializer(serializers.ModelSerializer):
    class Meta:
        model = NodeProperty
        fields = [
            'id',
            'property_name'
        ]


class NodePropertySerializer(BulkSerializerMixin, serializers.ModelSerializer):
    class Meta:
        model = NodeProperty
        # fields = [
        #     'id',
        #     'node',
        #     'property_name'
        # ]
        exclude = []
        list_serializer_class = BulkListSerializer


# NODE
class RefNodes(serializers.ModelSerializer):
    class Meta:
        model = NodeRef
        exclude = []


class NodeSerializer(serializers.ModelSerializer):
    properties = ListNodePropertySerializer(many=True, read_only=True)
    # refs = RefNodes(many=True,allow_null=True , read_only=True)
    reff = serializers.SerializerMethodField()
    ref_node = serializers.IntegerField(write_only=True, allow_null=True)

    # reff = RefNodes(many=True)

    class Meta:
        model = Node
        # fields = [
        #     'id',
        #     'tree',
        #     'node_name',
        #     'row_name_number',
        #     'node_horizontal_position',
        #     'properties'
        # ]
        exclude = []
        extra_kwargs = {
            'ref_node': {'write_only': True},
        }

    def get_reff(self, obj):
        objs = NodeRef.objects.filter(child=obj)
        return RefNodes(objs, many=True).data

    def create(self, validated_data):
        # print(validated_data)
        # print(*validated_data)
        tree = validated_data.get('tree')
        node_name = validated_data.get('node_name')
        ref_node = validated_data.get('ref_node')
        row_name_number = validated_data.get('row_name_number')
        node_horizontal_position = validated_data.get('node_horizontal_position')

        created_node = Node.objects.create(
            tree=tree,
            node_name=node_name,
            row_name_number=row_name_number,
            node_horizontal_position=node_horizontal_position
        )
        print('created_node: ', created_node)
        if ref_node:
            NodeRef.objects.get_or_create(
                node_ref_id=ref_node,
                child_id=created_node.id
            )
        return created_node


class NodeRefSerializer(serializers.ModelSerializer):
    class Meta:
        model = Node
        fields = [
            'id',
            'node_name'
        ]


class SingleNodeRefSerializer(serializers.ModelSerializer):
    class Meta:
        model = Node
        fields = [
            'id',
            'node_horizontal_position'
        ]


# TREE
class TreeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tree
        # fields = [
        #     'id',
        #     'tree_name'
        # ]
        exclude = []

# ROW
# class RowSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = Row
#         fields = [
#             'id',
#         ]
