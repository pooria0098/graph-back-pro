from django.urls import path

from nodes.views import (
    CreateTreeView,
    ListNodeView,
    CreateNodeView,
    NodePropertyView,
    ListNodePropertyView,
    ListNodeRefView, GetNodeRefView, ListTreeView
)

urlpatterns = [
    path('tree/create/', CreateTreeView.as_view()),
    path('tree/all/', ListTreeView.as_view()),
    # path('row/list/<int:tree_id>/', ListRowView.as_view()),
    path('node/create/', CreateNodeView.as_view()),
    path('node/list/<int:tree_id>/', ListNodeView.as_view()),
    path('node/ref/list/<int:tree_id>/', ListNodeRefView.as_view()),
    path('node/ref/<int:pk>/', GetNodeRefView.as_view()),
    # path('node/<int:pk>/', UpdateNodeView.as_view()),
    path('node-property/', NodePropertyView.as_view()),
    path('node-property/<int:node_id>/', ListNodePropertyView.as_view()),

]
