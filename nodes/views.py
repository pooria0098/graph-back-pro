from rest_framework.generics import (
    ListAPIView,
    RetrieveAPIView,
    CreateAPIView,
    DestroyAPIView,
    UpdateAPIView,
    ListCreateAPIView,

)
from rest_framework_bulk import (
    BulkUpdateAPIView,
    BulkCreateAPIView
)
from nodes.models import (
    Node,
    NodeProperty, Tree
)
from nodes.serializers import (
    TreeSerializer,
    NodeSerializer,
    NodePropertySerializer,
    ListNodePropertySerializer,
    NodeRefSerializer,
    SingleNodeRefSerializer,
)


# TREE
class CreateTreeView(CreateAPIView):
    serializer_class = TreeSerializer


class ListTreeView(ListAPIView):
    serializer_class = TreeSerializer
    queryset = Tree.objects.all()


# ROW
# class ListRowView(ListAPIView):
#     serializer_class = RowSerializer
#
#     def get_queryset(self):
#         return Row.objects.filter(tree_id=self.kwargs.get('tree_id'))

# NODE
class ListNodeView(ListAPIView):
    serializer_class = NodeSerializer

    def get_queryset(self):
        return Node.objects.filter(tree_id=self.kwargs.get('tree_id'))


class CreateNodeView(CreateAPIView):
    serializer_class = NodeSerializer


# class UpdateNodeView(UpdateAPIView):
#     serializer_class = NodeSerializer
#     queryset = Node.objects.all()

class ListNodeRefView(ListAPIView):
    serializer_class = NodeRefSerializer

    def get_queryset(self):
        return Node.objects.filter(tree_id=self.kwargs.get('tree_id'))


class GetNodeRefView(RetrieveAPIView):
    serializer_class = SingleNodeRefSerializer
    queryset = Node.objects.all()


# NODE PROPERTY
class ListNodePropertyView(ListAPIView):
    serializer_class = ListNodePropertySerializer

    def get_queryset(self):
        return NodeProperty.objects.filter(node_id=self.kwargs.get('node_id'))


class NodePropertyView(BulkCreateAPIView, BulkUpdateAPIView):
    serializer_class = NodePropertySerializer

    def get_queryset(self):
        # print(self.request.data)
        # print(self.request.data[0])
        # print(self.request.data[0]['node'])
        return NodeProperty.objects.filter(node_id=self.request.data[0]['node'])
